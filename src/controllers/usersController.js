const express = require('express');
const {User} = require('../models/userModel');
const {changeUsersPassword} = require('../services/usersService');
const {InvalidRequestError} = require('../utils/errors');
const {deleteUserProfile} = require('../services/usersService');
const {getUserInfo} = require('../services/usersService');
const {asyncWrapper} = require('../utils/apiUtils');
// eslint-disable-next-line
const router = express.Router();

router.get('/', asyncWrapper(async (req, res) => {
  const {userId} = req.user;
  const userInfo = await getUserInfo(userId);

  if (!userInfo) {
    throw new InvalidRequestError('No user with such id found!');
  }

  res.json({user: {
    _id: userInfo.id,
    username: userInfo.username,
    createdDate: userInfo.createdDate,
  },
  });
}));

router.delete('/', asyncWrapper(async (req, res) => {
  const {userId} = req.user;
  await deleteUserProfile(userId);
  res.json({'message': 'Success'});
}));

router.patch('/', asyncWrapper(async (req, res) => {
  const {userId} = req.user;
  let {newPassword} = req.body;
  const {oldPassword} = req.body;

  const user = await User.findOne({_id: userId});
  if (!oldPassword === user.password) {
    throw new InvalidRequestError('Invalid password');
  }

  await changeUsersPassword(userId, newPassword);
  return res.json({message: 'Success'});
}));
module.exports = {
  usersRouter: router,
};
