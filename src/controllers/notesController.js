const express = require('express');
const {toggleCompletedNote} = require('../services/notesService');

// eslint-disable-next-line
const router = express.Router();

const {
  getNotesByUserId,
  getNotesOfUserCount,
  getNoteByIdForUser,
  addNoteToUser,
  updateNoteByIdForUser,
  deleteNoteByIdForUser,
} = require('../services/notesService');

const {
  asyncWrapper,
} = require('../utils/apiUtils');
const {
  InvalidRequestError,
} = require('../utils/errors');

router.get('/', asyncWrapper(async (req, res) => {
  const {userId} = req.user;
  const offset = req.query.offset || 0;
  const limit = req.query.limit || 5;

  const notes = await getNotesByUserId(userId).skip(+offset).limit(+limit);
  const count = await getNotesOfUserCount(userId);
  const noteList = notes.map((note) => {
    return {
      _id: note._id,
      userId: userId,
      completed: note.completed,
      text: note.text,
      createdDate: note.createdDate,
    };
  });

  res.json(
      {
        offset,
        limit,
        count,
        notes: noteList,
      });
}));

router.get('/:id', asyncWrapper(async (req, res) => {
  const {userId} = req.user;
  const {id} = req.params;

  const note = await getNoteByIdForUser(id, userId);

  if (!note) {
    throw new InvalidRequestError('No note with such id found!');
  }

  const noteOutput = {
    _id: note._id,
    userId: userId,
    completed: note.completed,
    text: note.text,
    createdDate: note.createdDate,
  };

  res.json({note: noteOutput});
}));

router.post('/', asyncWrapper(async (req, res) => {
  const {userId} = req.user;

  await addNoteToUser(userId, req.body);

  res.json({message: 'Note created successfully'});
}));

router.put('/:id', asyncWrapper(async (req, res) => {
  const {userId} = req.user;
  const {id} = req.params;
  const text = req.body;

  await updateNoteByIdForUser(id, userId, text);

  res.json({message: 'Note updated successfully'});
}));

router.delete('/:id', asyncWrapper(async (req, res) => {
  const {userId} = req.user;
  const {id} = req.params;

  await deleteNoteByIdForUser(id, userId);

  res.json({message: 'Success'});
}));

router.patch('/:id', asyncWrapper( async (req, res) => {
  const {userId} = req.user;
  const {id} = req.params;

  await toggleCompletedNote(id, userId);
  res.json({message: 'Success'});
}));

module.exports = {
  notesRouter: router,
};
