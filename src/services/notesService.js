const {Note} = require('../models/noteModel');

const getNotesByUserId = (userId) => {
  return Note.find({userId});
};
const getNotesOfUserCount = (userId) => {
  return Note.find({userId}).count();
};

const getNoteByIdForUser = (noteId, userId) => {
  return Note.findOne({_id: noteId, userId});
};

const addNoteToUser = async (userId, notePayload) => {
  const note = new Note({...notePayload, userId});
  await note.save();
};

const updateNoteByIdForUser = async (noteId, userId, text) => {
  await Note.findOneAndUpdate({_id: noteId, userId}, {$set: text});
};

const deleteNoteByIdForUser = async (noteId, userId) => {
  await Note.findOneAndRemove({_id: noteId, userId});
};

const toggleCompletedNote = async (noteId, userId) => {
  const note = await Note.findOne({_id: noteId, userId});
  await Note.findOneAndUpdate(
      {
        _id: noteId, userId,
      },
      {
        $set:
        {
          completed: !note.completed,
        },
      });
};

module.exports = {
  getNotesByUserId,
  getNotesOfUserCount,
  getNoteByIdForUser,
  addNoteToUser,
  updateNoteByIdForUser,
  deleteNoteByIdForUser,
  toggleCompletedNote,
};
